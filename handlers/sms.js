const aws = require('../lib/aws');

/**
 * Send an SMS message
 *
 **/
module.exports.send = async function(event, context, callback){

  await aws.sendSMS('+1 774-272-2270', 'Your business card design is ready to review  https://www.vistaprint.com/graphic-design/190284-1341384-183498');

  return {
    statusCode: 200,
    headers: {
      'Content-Type': 'text/plain',
    },
    body: 'done',
  };
};

