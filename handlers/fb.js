const AWS = require("aws-sdk");
const axios = require("axios");
const debug = require("debug")("hatchery:facebook-page-api");

const CLIENT_ID = "225002344297686";
const CLIENT_SECRET = "3b0048e014fbab5ce4cc310221b3a935";

module.exports.search = async (event, context, callback) => {
  try {
    console.log("starting");
    const sourceIp = event.requestContext.identity.sourceIp;


    // No required query params
    if (!event.queryStringParameters.query && event.queryStringParameters.type) {
      callback(null, {
        statusCode: 400,
        body: "Query and type are required",
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Credentials": true
        },
      });
      return;
    }

    // invalid type query param
    if (!["page", "place"].includes(event.queryStringParameters.type)) {
      callback(null, {
        statusCode: 400,
        body: "Unsupported type",
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Credentials": true
        },
      });
      return;
    }

    // get geolocation from ip address if requested
    let lat = event.queryStringParameters.lat;
    let lon = event.queryStringParameters.lon;

    if(event.queryStringParameters.geolocate){
      try{
        let geoResp = await axios.get('http://api.ipstack.com/'+sourceIp+'?access_key=95567932b79cbf9a875dea85189bce8e');

        console.log("geo response: " + JSON.stringify(geoResp.data));
        console.log('ahhh: ' + geoResp.data.latitude);

        if(geoResp.data.latitude && geoResp.data.longitude){
          console.log("here");
          lat = geoResp.data.latitude;
          lon = geoResp.data.longitude;
        }
      }catch(e){
        console.log("error getting geo from ip ... continuing");
      }
    }


    console.log("I have queries!");
    const access_token = await getApplicationAccessToken();
    console.log("got token: " + access_token);

    var graphSearchUrl = `https://graph.facebook.com/v2.11/search?type=${
      event.queryStringParameters.type
      }&access_token=${access_token}&q=${encodeURIComponent(
        event.queryStringParameters.query
      )}&center=${lat},${lon}&fields=single_line_address,name,picture,category,fan_count&before=${event
          .queryStringParameters.before || ""}&after=${event
              .queryStringParameters.after || ""}&distance=50000&limit=${event
                  .queryStringParameters.limit || 5}`;

    if (!lon && !lat) {
      graphSearchUrl = `https://graph.facebook.com/v2.11/search?type=${
      event.queryStringParameters.type
    }&access_token=${access_token}&q=${encodeURIComponent(
      event.queryStringParameters.query
    )}&fields=single_line_address,name,picture,category,fan_count&before=${event
      .queryStringParameters.before || ""}&after=${event
          .queryStringParameters.after || ""}&limit=${event
              .queryStringParameters.limit || 5}`;
    }


    console.log("search url is: " + graphSearchUrl);
    let response = await axios.get(graphSearchUrl);
    console.log("got response");
    if (response.data.paging) {
      response.data.paging.next
        ? delete response.data.paging.next
        : null;
      response.data.paging.previous
        ? delete response.data.paging.previous
        : null;
    }

    console.log("returning response: " + JSON.stringify(response.data));

    return {
      statusCode: 200,
      body: JSON.stringify(response.data),
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true
      }
    };

  } catch (e) {
    console.log('EEK!: ' + e);
    callback(e, {
      statusCode: 500,
      body: e,
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true
      },
    });
  }
};

module.exports.place = (event, context, callback) => {
  try{
    if(event.queryStringParameters.pageId && event.queryStringParameters.pageId > 0){
      getApplicationAccessToken().then(access_token => {
        let requests = [];
        //first get the page info
        let pageInfoUrl = `https://graph.facebook.com/v2.11/${event.queryStringParameters.pageId}?access_token=${access_token}&fields=category,category_list,company_overview,contact_address,single_line_address,cover,current_location,description,description_html,emails,general_info,hours,is_always_open,link,location,name,phone,place_type,website,picture,about`;
        requests.push(axios.get(pageInfoUrl));
        //then get the large resolution profile images
        let largeResolutionProfileImgUrl = `https://graph.facebook.com/v2.11/${event.queryStringParameters.pageId}/photos?access_token=${access_token}&fields=source.width(9999)`;
        requests.push(axios.get(largeResolutionProfileImgUrl));
        
        axios.all(requests).then(axios.spread((infoResponse, imgProfileResponse)=>{
          //restructure and combine responses
          var images = [];
          if(imgProfileResponse.data && imgProfileResponse.data.data){
            imgProfileResponse.data.data.forEach((image) => {
              images.push(image.source);
            })
          }
          if(infoResponse.data.cover && infoResponse.data.cover.source){
            images.push(infoResponse.data.cover.source);
            delete infoResponse.data.cover;
          }
          var responseObj = {
            id: infoResponse.data.id,
            name: infoResponse.data.name,
            description: infoResponse.data.description || "",
            location: infoResponse.data.location,
            phone: infoResponse.data.phone,
            website: infoResponse.data.website || "",
            link: infoResponse.data.link,
            emails: infoResponse.data.emails,
            pictures: images,
            category: infoResponse.data.cateogry,
            about: infoResponse.data.about || ""
          }
          enhanceFacebookModel(responseObj).then((model)=>{
            callback(null, {
              statusCode: 200,
              body: JSON.stringify(model),
              headers: {
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true
              },
            })
          })
        })).catch(err =>{ 
          console.log(err);
        });
      }).catch(err => {
        console.log(err);
        callback(null, {
          statusCode: 502,
          body: "Zucks",
          headers: {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Credentials": true
          },
        });
      })
    }else{
      callback(null, {
        statusCode: 400,
        body: "pageId required",
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Credentials": true
        },
      })
    }
  }catch(e){
    callback(e, {
      statusCode: 500,
      body: e,
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true
      },
    })
  }
};

function getApplicationAccessToken() {
  return new Promise((resolve, reject) => {
    axios
      .get(
        `https://graph.facebook.com/v2.11/oauth/access_token?client_id=${CLIENT_ID}&client_secret=${CLIENT_SECRET}&grant_type=client_credentials`
      )
      .then(response => {
        resolve(response.data["access_token"]);
      })
      .catch(err => {
        resolve(err);
      });
  });
}

function enhanceFacebookModel(model){
  return new Promise((resolve, reject)=> {
    if(model.about.length < model.description.length){
      model.description = model.about
    }
    if(model.website){
      let websites = model.website.split(' ');
      var newWebsiteObj = {};
      websites.forEach((website) => {
        if(website.includes("facebook.com")){
          newWebsiteObj[website] = "facebook";
        }
        else if(website.includes("twitter.com")){
          newWebsiteObj[website] = "twitter";
        }
        else{
          newWebsiteObj[website] = "company";
        }
      })
      model.website = newWebsiteObj;
    }
    resolve(model);
  })
}
