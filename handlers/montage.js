'use strict';

var s3Utils = require('../lib/s3utils.js');
var imageUtils = require('../lib/imageutils');
var http = require('http');


module.exports.calendar = async function(event, context, callback){
  const altDocId = event.queryStringParameters.altDocId;
  const product = event.queryStringParameters.product;

  await generateCalendarPreview(altDocId, product);

  return({
    statusCode: 200,
    headers: {
    },
  });
};

module.exports.getCalendar = async function(event, context, callback){
  const altDocId = event.queryStringParameters.altDocId;
  const product = event.queryStringParameters.product;

  let results = await s3Utils.listObjects(`montage/${altDocId}/`);
  let objects = results.Contents;

  if(objects.length === 0){
    console.log(objects)
    await generateCalendarPreview(altDocId, product);
    results = await s3Utils.listObjects(`montage/${altDocId}/`);
    objects = results.Contents;
  }

  let publicUrl = '';

  objects.sort(function(a,b) {
    return new Date(a.LastModified) - new Date(b.LastModified)
  })
  
  while(publicUrl === ''){
    if(objects.length <= 0 ){
      return({
        statusCode: 500
      });
    }

    let s3Object = objects.pop();
    if(s3Object.Size !== 0){
      publicUrl = `https://s3.amazonaws.com/hatchery-lambda-utils/${s3Object.Key}`
    }
  }
  console.log(objects);
  return({
    statusCode: 302,
    headers: {
      Location: publicUrl,
    },
  });
};

module.exports.createXsellMontage = async function(event, context, callback){
  const altDocId = event.queryStringParameters.incoming;
  const carousel = event.queryStringParameters.car.split(',').filter((el) => {return el !== ' ' && el !== '' && el !== null});
  const tiles = event.queryStringParameters.tiles.split(',').filter((el) => {return el !== ' ' && el !== '' && el !== null});

  await generateXsellPreview(altDocId, carousel, tiles);

  return({
    statusCode: 200,
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Credentials": true
    },
  });
};

module.exports.getXsellMontage = async function(event, context, callback){
  const altDocId = event.queryStringParameters.altDocId;

  let results = await s3Utils.listObjects(`montage/${altDocId}/`);
  let objects = results.Contents;

  if(objects.length === 0){
    return({
      statusCode: 404,
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true
      },
    });
  }

  let publicUrl = '';

  objects.sort(function(a,b) {
    return new Date(a.LastModified) - new Date(b.LastModified)
  })
  
  while(publicUrl === ''){
    if(objects.length <= 0 ){
      return({
        statusCode: 500
      });
    }

    let s3Object = objects.pop();
    if(s3Object.Size !== 0){
      publicUrl = `https://s3.amazonaws.com/hatchery-lambda-utils/${s3Object.Key}`
    }
  }
  console.log(objects);
  return({
    statusCode: 302,
    headers: {
      Location: publicUrl,
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Credentials": true
    },
  });
};


async function generateCalendarPreview(altDocId, product){
  return new Promise(async (resolve) =>{
    let redirect = '';
  
    if(product === '315'){
      redirect = await imageUtils.deskCalendarMontage(altDocId);
    }
    else{
      redirect = await imageUtils.calendarMontage(altDocId);
    }
    http.get(redirect, function(res){
      var imageData = '';
      res.setEncoding('binary');
  
      res.on('data', function(chunk){
        imageData += chunk;
      });
  
      res.on('end', function(){
        s3Utils.upload(
          `montage/${altDocId}/${Date.now()}.png`,
          'image/png',
          new Buffer(imageData, 'binary')
        ).then(()=>{
          resolve();
        });
      })
    })
  })
}

async function generateXsellPreview(altDocId, carousel, tiles){
  return new Promise(async (resolve) =>{
    let redirect = '';

    redirect = await imageUtils.xsellMontage(altDocId, carousel, tiles)
    http.get(redirect, function(res){
      var imageData = '';
      res.setEncoding('binary');
  
      res.on('data', function(chunk){
        imageData += chunk;
      });
  
      res.on('end', function(){
        s3Utils.upload(
          `montage/${altDocId}/${Date.now()}.png`,
          'image/png',
          new Buffer(imageData, 'binary')
        ).then(()=>{
          resolve();
        });
      })
    })
  })
}