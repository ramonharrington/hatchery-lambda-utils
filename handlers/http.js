'use strict';

const request = require('request');
var httpUtils = require('../lib/httpUtils.js');
let axios = require('axios');


module.exports.checkSSL = function(event, context, callback){
  const host = event.queryStringParameters.host;

  console.log("host is: " + host);
  httpUtils.getSSLCertificate(host).then( resp => {
    console.log("response: " + JSON.stringify(resp));

    callback(null, {
      statusCode: 200,
      headers: {
        "Access-Control-Allow-Origin" : "*",
        "Access-Control-Allow-Credentials" : true
      },
      body: JSON.stringify(resp),
    });
  });
}

module.exports.corsit = async function(event, context, callback){
  let params = event.queryStringParameters;

  console.log(event);
  console.log(`Got request with params:`, params);

  if (!params.url) {
    const errorResponse = {
      statusCode: 400,
      body: 'Unable get url from \'url\' query parameter'
    };

    callback(null, errorResponse);

    return;
  }

  return new Promise((resolve) => {
    axios.request({
      url: params.url,
      method: event.httpMethod,
      headers: {'content-type' : event.headers['Content-Type']},
      data: event.body || null,
    }).then((response) => {
      console.log(`Got response from ${params.url} ---> {statusCode: ${response.statusCode}}`);

      const proxyResponse = {
        statusCode: response.statusCode,
        headers: {
          "Access-Control-Allow-Origin" : "*",
          "Access-Control-Allow-Credentials" : true,
          "content-type": response.headers['content-type']
        },
        body: JSON.stringify(response.data)
      };

      resolve(proxyResponse);
    }).catch((error) => {
        console.log(`Got error`, error);
        const proxyResponse = {
          statusCode: error.response.status,
          headers: {
            "Access-Control-Allow-Origin" : "*",
            "Access-Control-Allow-Credentials" : true,
            "content-type": error.response.headers['content-type']
          },
          body: error.response.data
        };
        resolve(proxyResponse);
        return;
    });
  });
}

module.exports.corsimage = async function(event, context, callback){
  let params = event.queryStringParameters;

  console.log(event);
  console.log(`Got request with params:`, params);

  if (!params.url) {
    const errorResponse = {
      statusCode: 400,
      body: 'Unable get url from \'url\' query parameter'
    };

    callback(null, errorResponse);

    return;
  }

  return new Promise((resolve, reject) => {
    axios.request({
      responseType: 'arraybuffer',
      url: params.url,
      method: 'get',
    }).then((response)=>{
      const proxyResponse = {
        statusCode: response.statusCode,
        headers: {
          "Access-Control-Allow-Origin" : "*",
          "Access-Control-Allow-Credentials" : true,
          "content-type": response.headers['content-type']
        },
        body: new Buffer(response.data, 'binary').toString('base64'),
        isBase64Encoded : true,
      };

      resolve(proxyResponse);
    }).catch((error)=> {
      reject(error);
      return
    });
  });
}

