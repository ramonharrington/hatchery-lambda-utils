'use strict';

var templateUtils = require('../lib/templateUtils.js');
var s3Utils = require('../lib/s3utils.js');
var imageUtils = require('../lib/imageutils');

const axios = require('axios');


module.exports.getProductScene = async function(event, context, callback){
  const type = event.queryStringParameters.type;
  const altdocid = event.queryStringParameters.altdocid;
  const background = event.queryStringParameters.bkg;

  // Get image of product a little less than full height of the facebook image size
  let src='http://vistaprint.com/vp/ns/livepreview.aspx?alt_doc_id='+altdocid+'&height=600&png=1';

  let preview = await templateUtils.genScene(type, background, src);
  let s3url = await s3Utils.uploadScene(preview);

  return({
    statusCode: 302,
    headers: {
      Location: s3url,
    },
  });
}

module.exports.phoneCrop = function(event, context, callback){
    let body = event.body;
    
    console.log(body);
    templateUtils.phoneCrop(body).then( preview => {
        console.log(preview);

        const response = {
            statusCode: 200,
            headers: {
                "Access-Control-Allow-Origin": "*"
            },
            body: JSON.stringify({"data": preview})
        };

        callback(null, response);
    });
}


module.exports.getPhoneMask = async function(event, context, callback){
  const maskUrl = event.queryStringParameters.mask;
  const filename = event.queryStringParameters.filename || 'temp.png';

  let mask = await templateUtils.phoneMaskRecolor(maskUrl);
  let s3url = await s3Utils.upload('masks/'+filename, 'image/png', mask);
  let response = {
    statusCode: 200,
    headers: {
      "Access-Control-Allow-Origin": "*"
    },
    body: s3url,
  };

  return response;
};



