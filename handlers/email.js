var SparkPost = require('sparkpost');
var client = new SparkPost('2628a2837c2a9ba8fce2de3674be1d710fe9df7a');
const querystring = require('querystring');

const axios = require('axios');
const s3utils = require('../lib/s3utils.js');
const dbutils = require('../lib/dbutils');
const slackUtils = require('../lib/slack');
const ccUtils = require('../lib/ccUtils');
const tinypng = require('../lib/tinypng');
const simpleParser = require('mailparser').simpleParser;



async function processUserTestingWebPlayer(web_player){
  let page = await axios.get(web_player);

  let json = page.data.match(/.*BootstrapDataProvider.setKeys\((.*)\)/)[1];
  let j = JSON.parse(json);

  console.log("temp url: " + j.video._links.temporary_url);

  let title = '';
  let body = '';
  let vid_time = j.test_tasks.reduce( (a,b) => { return a > b.end_time ? a : b.end_time })

  let key = `slackbridge/usertesting/parsed/${j.session.reference_id}`;

  console.log('uploading video');
  let video = await axios.get(j.video._links.temporary_url.href, {responseType: 'arraybuffer'});
  await s3utils.upload(`${key}.mp4`, 'video/mp4', Buffer.from(video.data));

  console.log('uploading poster');
  let preview = await axios.get(j.video._links.secure_poster_full_url.href, {responseType: 'arraybuffer'});
  await s3utils.upload(`${key}.jpg`, 'image/jpeg', Buffer.from(preview.data));

  return {
    web_player,
    title,
    body,
    poster_image: 'https://s3.amazonaws.com/hatchery-lambda-utils/' + key + '.jpg',
    video_link: 'https://s3.amazonaws.com/hatchery-lambda-utils/' + key + '.mp4',
    duration: Math.round(vid_time/60*100)/100,
    transcript_link: j.transcript ? j.transcript.signed_text_url : null,
  };

}

/**
 * Continue from another device sparkpost email
 *
 **/
module.exports.continueFromAnotherDevice = async function(event, context, callback){
  console.log(event.body);
  console.log(JSON.stringify(event.body));
  let data = JSON.parse(event.body);
  
  if(!data.email) {
    return {
      statusCode: 400,
      body: JSON.stringify("Bad Request"),
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true
      },
    };
  }

  let endcodedEditUrl = encodeURIComponent(data.editUrl);
  let redirectEditUrl = `https://utils.hatchery.vistaprint.com/prod/emails/redirectEditLink?editUrl=${endcodedEditUrl}`;

  var transmission = {
    campaign_id: 'continue-from-another-device',
    content: {
      template_id: 'continue-from-another-device'
    },
    recipients: [{
      address: { email: data.email },
      substitution_data: {
        edit_url: redirectEditUrl,
        preview_url: data.previewUrl,
        studioEmailBookmarkEmailHeading: data.StudioEmailBookmarkEmailHeading,
        studioEmailBookmarkEmailBody: data.StudioEmailBookmarkEmailBody,
        studioEmailBookmarkEmailSubject: data.StudioEmailBookmarkEmailSubject,
        studioEmailBookmarkEmailCta: data.StudioEmailBookmarkEmailCta
      },
    }], 
  };

  console.log("Sending to: " + data.email);
  console.log(JSON.stringify(data));
  await client.transmissions.send(transmission);

  // Log the event
  await dbutils.insert('coad', {
    email: data.email,
    referer: event.headers.Referer ? event.headers.Referer.substring(0,100) : null,
    session_id: data.session,
    product: data.pfid,
    edit_url: data.editUrl ? data.editUrl.substring(0,100) : null,
    preview_url: data.previewUrl ? data.previewUrl.substring(0,100) : null,
    studioEmailBookmarkEmailHeading: data.StudioEmailBookmarkEmailHeading,
    studioEmailBookmarkEmailBody: data.StudioEmailBookmarkEmailBody,
    studioEmailBookmarkEmailSubject: data.StudioEmailBookmarkEmailSubject,
    studioEmailBookmarkEmailCta: data.StudioEmailBookmarkEmailCta,
  });

  return {
    statusCode: 200,
    body: JSON.stringify("Success"),
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Credentials": true
    },
  };
}

module.exports.redirectEditLink = async function(event, context, callback){
  let editUrl = event.queryStringParameters.editUrl;
  editUrl = decodeURIComponent(editUrl);
  console.log("User clicked edit link in email. ");

  //TODO: Record this in database.

  return {
    statusCode: 302,
    body: JSON.stringify("Success"),
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Credentials": true,
      "Location": editUrl
    },
  };
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

module.exports.slackUsertestingHook = async function(event, context, callback){
  // let key = 'slackbridge/usertesting/raw/m3pr320jkquuu3djlc85l9q7ponn823a6375oog1';

  // sleep 60 seconds to give usertesting time to transcribe/transcode/etc all the assets
  await sleep(120 * 1000);


  let key = unescape(event.Records[0].s3.object.key);
  let obj = await s3utils.getObject('hatchery-lambda-utils', key);
  let mail = await simpleParser(obj.Body);
  let slack = slackUtils.getBaseData('vistaprint');

  console.log("subject: " + mail.subject);

  let user_channel = mail.subject.match(/#(\w|-)+/);
  console.log("user channel is: " + JSON.stringify(user_channel));
  if(user_channel){
    console.log("setting user channel to " + user_channel[0]);
    slack.channel = user_channel[0];
  }else{
    slack.channel = '#jontest';
  }

  let web_player = mail.html.match(/(https:\/\/www.usertesting.com\/v\/.*\?encrypted_video_handle=.*?)&/)[1]

  // Split out the title and body from the email subject
  let splits = mail.subject.split(':');
  let title = splits.shift();
  let body = splits.join(' ');



  try{
    let utdata = await processUserTestingWebPlayer(web_player);

    slack.username = 'User Testing - hatchdata';
    slack.icon_emoji = ':usertesting:';

    slack.attachments = [{
      title: title,
      text: body,
      title_link: web_player,
      thumb_url: utdata.poster_image,
      fields: [
        {
          title: 'Video',
          value: `<${utdata.video_link}|watch>`,
          'short': true,
        },
        {
          title: 'Transcript',
          value: utdata.transcript_link ? `<${utdata.transcript_link}|download>` : 'not available',
          'short': true,
        },
        {
          title: 'Duration',
          value: `${utdata.duration} min`,
          'short': true,
        },
      ],
    }];
    await slackUtils.send(slack);

  }catch(ex){
    // If something goes wrong, just send the watch link
    slack.text = `*${mail.subject}* - ${web_player} - ${ex}`;

    await slackUtils.send(slack);
  }

  return({
    statusCode: 200,
    body: 'done',
  });
}


module.exports.slackMailHook = async function(event, context, callback){
  let key = unescape(event.Records[0].s3.object.key);
  let obj = await s3utils.getObject('hatchery-lambda-utils', key);
  let mail = await simpleParser(obj.Body);

  console.log("subject: " + mail.subject);
  console.log("attachments: " + mail.attachments.length);

  for(let i=0; i<mail.attachments.length; i++){
    let attachment = mail.attachments[i];

    if(attachment.contentType === 'application/pdf'){
      let s3file = '' + Date.now() + '_' + attachment.filename;
      let s3key = 'slackbridge/emails/parsed/' + s3file;
      let url = await s3utils.upload(
        s3key,
        attachment.contentType,
        attachment.content);
      console.log("url: " + url);

      let slack = slackUtils.getBaseData('vistaprint');
      let user_channel = mail.subject.match(/#(\w|-)+/);
      console.log("user channel is: " + JSON.stringify(user_channel));
      if(user_channel){
        console.log("setting user channel to " + user_channel[0]);
        slack.channel = user_channel[0];
      }
      slack.attachments = [];

      console.log("We are looking at a pdf");
      let jpgUrl = await ccUtils.convertPdf(url, s3key+'.jpg');
      console.log("Got jpg url: " + jpgUrl);

      console.log("sending over to tinypng");
      let compressedFolder = 'hatchery-lambda-utils/slackbridge/emails/parsed/compressed/';
      await tinypng.webToBucket(jpgUrl, compressedFolder+s3file+'.jpg');

      slack.text = mail.subject;
      slack.attachments.push({
        title: attachment.filename,
        title_link: url,
        image_url: 'https://s3.amazonaws.com/hatchery-lambda-utils/slackbridge/emails/parsed/compressed/'+encodeURIComponent(s3file)+'.jpg',
      });

      console.log("sending slack: " + JSON.stringify(slack));
      await slackUtils.send(slack);
      console.log("done sending slack");
    }
  }

  console.log("Done");
}


/**
 * Sparkpost incoming webhook
 *
 **/
module.exports.sparkpostIncoming = async function(event, context, callback){
  let data = JSON.parse(event.body);
  
  for(let i=0; i<data.length; i++){
    if(!data[i].msys){
      console.log("no data: " + JSON.stringify(data));
      continue;
    }

    let record = data[i].msys.track_event || data[i].msys.message_event;

    if(!record){
      console.log("no record: " + JSON.stringify(data));
      continue;
    }

    if(record.geo_ip){
      record.geo_ip = `${record.geo_ip.city}, ${record.geo_ip.region}  ${record.geo_ip.country}`;
    }
    record.rcpt_meta = JSON.stringify(record.rcpt_meta);
    record.rcpt_tags = record.rcpt_tags.join(',');

    try{
      await dbutils.insert('sparkpost_incoming', record);
    }catch(ex){
      await slackUtils.sendDataMsg({
        channel: '#jontest',
        text: 'sparkpostIncoming - error - ' + ex
      });
      throw ex;
    }
  }


  return {
    statusCode: 200,
    body: 'ok',
  };

};

