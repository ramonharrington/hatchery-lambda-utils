const signup = require('../lib/research_signup');
const slackutils = require('../lib/slack.js');
const axios = require('axios');
const querystring = require('querystring');


function slackEvent(name, business, phone, email){

  var data = JSON.stringify({
    channel: "research-signups",
    username: "hatchery",
    icon_url: "http://hatchery.vistaprint.com/images/h.png",
    text: "New signup received",
    "attachments":[
      {
        fields: [
          {
            title: 'Name',
            value: name,
            short: true,
          },{
            title: 'Business',
            value: business,
            short: true,
          },{
            title: 'Phone',
            value: phone,
            short: true,
          },{
            title: 'Email',
            value: email,
            short: true,
          }
        ]
      }
    ]
  });

  return new Promise( (resolve) => {
    // should just return the data obj and use the slack lib -- but too lazy right now
    axios.post('https://hooks.slack.com/services/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', data)
      .then( (resp) => {
        console.log("Posted new signup to slack");
        resolve();
      }).catch( (err) => {
        console.log("Error posting new signup to slack: " + err);
      });;
  });
}


module.exports.researchSignup = async function(event, context, callback){
  let body = querystring.parse(event.body);
  console.log("new signup received: " + JSON.stringify(body));

  //signup.addSignup(event.body);
  await signup.addSignup(body);
  await slackEvent(body.name, body.business, body.phone, body.email);

  console.log("Name: " + body.name);

  return{
    statusCode: 200,
    body: JSON.stringify({status: 'ok'}),
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Credentials": true,
    }
  };

}
