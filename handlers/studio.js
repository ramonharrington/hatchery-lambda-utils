const ddbUtils = require('../lib/ddbUtils');

module.exports.isOurStudio = async function(event, context, callback){
  let docid = event.queryStringParameters.docid;
  let isOurs = await ddbUtils.isNewStudioDocId(docid);

  return {
    statusCode: 200,
    body: JSON.stringify({status: isOurs}),
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Credentials": true
    },
  };
}


module.exports.setOurStudio = async function(event, context, callback){
  let docid = event.queryStringParameters.docid;
  await ddbUtils.putStudioDocId(docid);

  return {
    statusCode: 200,
    body: JSON.stringify({status: 'success'}),
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Credentials": true
    },
  };
}
