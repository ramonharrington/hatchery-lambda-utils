var AWS = require('aws-sdk');
AWS.config.update({region: 'us-east-1'});
var ddb = new AWS.DynamoDB({apiVersion: '2012-10-08'});


module.exports = class DDBUtils{

  static putStudioDocId(docId){
    var params = {
      TableName: 'mobile-studio-documents',
      Item: {
        'docid' : {S: docId},
        'created' : {S: new Date().getTime().toString()},
      }
    };

    return new Promise( async (resolve, reject) => {
      if(await this.isNewStudioDocId(docId)){
        resolve();
        return;
      }

      ddb.putItem(params, function(err, data) {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  }

  static isNewStudioDocId(docid){
    var params = {
      TableName: 'mobile-studio-documents',
      Key: {
        'docid' : {S: docid},
      },
    };

    return new Promise( resolve => {
      ddb.getItem(params, function(err, data) {
        if (err) {
          console.log("ERRROR!: " + err);
          resolve(false);
        } else {
          resolve(data && typeof data.Item !== 'undefined');
        }
      });
    });
  }
}


