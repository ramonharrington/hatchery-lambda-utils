var cloudinary = require('cloudinary');
const s3utils = require('./s3utils');


cloudinary.config({
  cloud_name: 'hatchery',
  api_key: '255897247756768',
  api_secret: 'b-eDMTwB9ajS6aoNYnfpyVRm9RM',
});


function upload(url) {
  return new Promise( (resolve) => {
    cloudinary.uploader.upload(url, function(result) { 
      //console.log("result: " + JSON.stringify(result.public_id));
      resolve(result.public_id);
    });
  });
}


module.exports.calendarMontage = async (altdocid) => {
  //let altdocid = '1RF0W-05A18-7J3';
  let urls = [];
  for(let i=1; i<=26; i++){
    let baseurl = 'http://www.vistaprint.com/any/lp.aspx?width=300&png=1&alt_doc_id='+altdocid+'&page=';
    urls.push(baseurl + i);
  }
  let promises = urls.map( (url) => { return upload(url) } );
  let images = await Promise.all(promises);

  //console.log("total images: " + images.length);
  //effect: "shadow:40", 

  let rowHeight = 240;

  let ySpacing = rowHeight;  // counter var

  return new Promise( (resolve) => {
    let transformation = [];

    transformation.push({gravity: 'north', overlay: images[1], width: 300, x: -305, y: ySpacing, crop: "fit"});
    transformation.push({gravity: 'north', overlay: images[3], width: 300, x: 153,  y: ySpacing, crop: "fit"});
    transformation.push({gravity: 'north', overlay: images[5], width: 300, x: 458,  y: ySpacing, crop: "fit"});

    ySpacing += rowHeight;

    transformation.push({gravity: 'north', overlay: images[7],  width: 300, x: -305, y: ySpacing, crop: "fit"});
    transformation.push({gravity: 'north', overlay: images[9],  width: 300, x: 0,  y: ySpacing, crop: "fit"});
    transformation.push({gravity: 'north', overlay: images[11], width: 300, x: 305,  y: ySpacing, crop: "fit"});

    ySpacing += rowHeight;

    transformation.push({gravity: 'north', overlay: images[13], width: 300, x: -305, y: ySpacing, crop: "fit"});
    transformation.push({gravity: 'north', overlay: images[15], width: 300, x: 0,    y: ySpacing, crop: "fit"});
    transformation.push({gravity: 'north', overlay: images[17], width: 300, x: 305,  y: ySpacing, crop: "fit"});

    ySpacing += rowHeight;

    transformation.push({gravity: 'north', overlay: images[19], width: 300, x: -305, y: ySpacing, crop: "fit"});
    transformation.push({gravity: 'north', overlay: images[21], width: 300, x: 0,    y: ySpacing, crop: "fit"});
    transformation.push({gravity: 'north', overlay: images[23], width: 300, x: 305,  y: ySpacing, crop: "fit"});

    ySpacing += rowHeight;

    transformation.push({gravity: 'north', overlay: images[25], width: 300, x: 0,  y: ySpacing, crop: "fit"});

    let montage = cloudinary.url(images[0], {transformation});

    console.log('montage: ' + montage);
    resolve(montage);
  });
};

module.exports.deskCalendarMontage = async (altdocid) => {
  //let altdocid = '1RF0W-05A18-7J3';
  let urls = [];
  for(let i=1; i<=14; i++){
    let baseurl = 'http://www.vistaprint.com/any/lp.aspx?width=300&png=1&alt_doc_id='+altdocid+'&page=';
    urls.push(baseurl + i);
  }
  let promises = urls.map( (url) => { return upload(url) } );
  let images = await Promise.all(promises);

  //console.log("total images: " + images.length);
  //effect: "shadow:40", 

  let rowHeight = 240;

  let ySpacing = rowHeight;  // counter var

  return new Promise( (resolve) => {
    let transformation = [];

    transformation.push({gravity: 'north', overlay: images[1], width: 300, x: -305, y: ySpacing, crop: "fit"});
    transformation.push({gravity: 'north', overlay: images[2], width: 300, x: 153,  y: ySpacing, crop: "fit"});
    transformation.push({gravity: 'north', overlay: images[3], width: 300, x: 458,  y: ySpacing, crop: "fit"});

    ySpacing += rowHeight;

    transformation.push({gravity: 'north', overlay: images[4],  width: 300, x: -305, y: ySpacing, crop: "fit"});
    transformation.push({gravity: 'north', overlay: images[5],  width: 300, x: 0,  y: ySpacing, crop: "fit"});
    transformation.push({gravity: 'north', overlay: images[6], width: 300, x: 305,  y: ySpacing, crop: "fit"});

    ySpacing += rowHeight;

    transformation.push({gravity: 'north', overlay: images[7], width: 300, x: -305, y: ySpacing, crop: "fit"});
    transformation.push({gravity: 'north', overlay: images[8], width: 300, x: 0,    y: ySpacing, crop: "fit"});
    transformation.push({gravity: 'north', overlay: images[9], width: 300, x: 305,  y: ySpacing, crop: "fit"});

    ySpacing += rowHeight;

    transformation.push({gravity: 'north', overlay: images[10], width: 300, x: -305, y: ySpacing, crop: "fit"});
    transformation.push({gravity: 'north', overlay: images[11], width: 300, x: 0,    y: ySpacing, crop: "fit"});
    transformation.push({gravity: 'north', overlay: images[12], width: 300, x: 305,  y: ySpacing, crop: "fit"});

    ySpacing += rowHeight;

    transformation.push({gravity: 'north', overlay: images[13], width: 300, x: 0,  y: ySpacing, crop: "fit"});

    let montage = cloudinary.url(images[0], {transformation});

    console.log('montage: ' + montage);
    resolve(montage);
  });
};

module.exports.xsellMontage = async (incomingDoc, carousel, tiles) => {
  let incomingDocUrl = `https://www.vistaprint.com/any/lp.aspx?width=300&png=1&alt_doc_id=${incomingDoc}&page=1`

  let carUrls = [];
  carousel.forEach((rec) => {
    carUrls.push(`https://www.vistaprint.com/any/lp.aspx?width=300&png=1&doc_sig=${rec}&page=1`)
  });

  let tilesUrls = [];
  tiles.forEach((rec) => {
    tilesUrls.push(`https://www.vistaprint.com/any/lp.aspx?width=300&png=1&doc_sig=${rec}&page=1`)
  });

  let uIncoming = await upload(incomingDocUrl);
  let uCarUrl = await Promise.all(carUrls.map((url) => { return upload(url) }));
  let uTilesUrl = await Promise.all(tilesUrls.map((url) => { return upload(url) }));

  let rowHeight = 300;

  let ySpacing = rowHeight;  // counter var

  return new Promise( (resolve) => {
    let transformation = [];

    //carousel 
    let xPos = 0;
    for(let x = 0; x < uCarUrl.length; x++){
      transformation.push({gravity: 'north_west', overlay: uCarUrl[x], width: 300, height: 300, x: xPos, y: ySpacing, crop: "fit"});
      xPos += 300
    }

    xPos = 0;
    for(let x = 0; x < uTilesUrl.length; x++){
      transformation.push({gravity: 'north_west', overlay: uTilesUrl[x], width: 300, height: 300, x: xPos, y: ySpacing * 2, crop: "fit"});
      xPos += 300     
    }

    let montage = cloudinary.url(uIncoming, {transformation});

    console.log('montage: ' + montage);
    resolve(montage);
  });
};