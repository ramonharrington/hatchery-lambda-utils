var axios = require('axios');

/** Profiles with default values **/
const profiles = {
  hatchery: {
    webhook: 'https://hooks.slack.com/services/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',
    channel: '#dev-notifications',
    username: 'hatchery',
    icon_emoji: ':hatchery:',
  },
  vistaprint: {
    webhook: 'https://hooks.slack.com/services/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',
    channel: '#hatchery-data',
    username: 'hatchdata',
    icon_emoji: ':hatchery:',
  },
};

module.exports = {
  getBaseData: function(profile = 'hatchery'){
    if(!profile || !profiles[profile]){
      return profiles.hatchery;
    }

    return profiles[profile];
  },

  send: async function(data){
    await axios.post(data.webhook, data);
  },

  sendMsg: async function(msg){
    var data = this.getBaseData();
    data.text = msg;

    await this.send(data);
  },

  sendDataMsg: async function(msg){
    var data = this.getBaseData();
    Object.assign(data, msg);

    await this.send(data);
  },
};

