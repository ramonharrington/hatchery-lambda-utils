var sql = require('mssql');
var fs = require('fs');

var sqlconfig = {
  'analytics-prod': {
    user: "admin",
    password: "Hatchery!23",
    server: "analytics-dev-1.cigogjiyqq2f.us-east-1.rds.amazonaws.com",
    database: "analytics",
    requestTimeout: 300000,
  },
};


function insert(table, obj){
  var values = [];

  for(let i=0; i<Object.keys(obj).length; i++){
    var val = obj[Object.keys(obj)[i]];
    if(val === '' || val === null || typeof val === 'undefined'){
      val = 'null'
    } else if(typeof val.getDate !== 'undefined'){
      val = val.getFullYear() + '-' + (val.getMonth()+1) + '-' + val.getDate() + ' ' + val.getHours() + ':' + val.getMinutes();
    }

    if(val !== null && (typeof val === 'string' || Array.isArray(val)) ){
      if (typeof val !== 'string') {
        val = val.toString();
      }
      val = val.replace(/'/g, "''");
      values.push("'" + val + "'");
    } else if((typeof val === 'boolean') && val !== null){
      val = val ? 1 : 0;
      values.push(val);
    } else{
      values.push(val);
    }
  }

  var query = 'insert into ' + table + '(' + Object.keys(obj).join(',') + ') values(' + values.join(',') + ');\n';
  console.log("query is: " + query);
  return query;
}



module.exports = {
  insert: async function(table, obj){
    sql.close();
    await sql.connect(sqlconfig['analytics-prod']);


    let recordset = await new sql.Request().query(insert(table, obj));
    sql.close();

    return recordset;
  },

  bulkInsert: async function(table, collection, errorCallback){
    sql.close();
    await sql.connect(sqlconfig['analytics-prod']);
    let results = [];
    for (const obj of collection)
    {
      let recordset = await new sql.Request()
        .query(insert(table, obj))
        .catch(err => {
          if (errorCallback){
            errorCallback(obj, err);
          }
        });
      if (recordset)
      {
        results.push(recordset);
      }
    }

    sql.close();
    return results;
  },

  exec: function(dbname, query, parameters, callback){
    return new Promise( async (resolve) => {
      /*
       * Replace any sql variables we need
       */
      sql.close();
      await sql.connect(sqlconfig[dbname]);
      let request = new sql.Request();
      if (parameters) {
        for (const [key, value] of Object.entries(parameters))
        {
          request.input(key, value);
        }
      }
      let recordset = await request.query(query);
      var json = [
      ];

      if(recordset.recordset.length > 0){

        for(let i=0; i<recordset.recordset.length; i++){

          var obj = {};

          for(let j=0; j<Object.keys(recordset.recordset[i]).length; j++){
            let key = Object.keys(recordset.recordset[i])[j];
            obj[key] = ''+recordset.recordset[i][key];
          }
          json.push(obj);
        }
      }

      sql.close();
      if(callback){
        callback(json);
      }
      resolve(json);
    });
  },
};
