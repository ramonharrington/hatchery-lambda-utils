const uuidv4 = require('uuid/v4');
const AWS = require('aws-sdk');

const BUCKET_NAME = 'hatchery-lambda-utils';
const IAM_USER_KEY = 'AKIAJ3SMZNBZEXGX72RA';
const IAM_USER_SECRET = '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';
const BUCKET_PATH_URL = "https://s3.amazonaws.com/hatchery-lambda-utils/";

let s3bucket = new AWS.S3({
    accessKeyId: IAM_USER_KEY,
    secretAccessKey: IAM_USER_SECRET,
    Bucket: BUCKET_NAME
});

module.exports.getObject = function(bucket, key){
  var params = {
    Bucket: bucket,
    Key: key,
  };

  return new Promise( resolve => {
    s3bucket.getObject(params, (error, data) => {
      if (error) {
        console.log(error);
      } else {
        console.log("successfully got object from s3");
      }

      resolve(data);
    });
  });
};

module.exports.listObjects = function(prefix){
  var params = {
    Bucket: BUCKET_NAME,
    Prefix: prefix,
  };

  return new Promise( resolve => {
    s3bucket.listObjectsV2(params, (error, data) => {
      if (error) {
        console.log(error);
      } else {
        console.log("successfully got objects from s3");
      }

      resolve(data);
    });
  });
}

module.exports.upload = function (key, contentType, data){
  var params = {
    Bucket: BUCKET_NAME,
    Key: key,
    Body: data,
    ContentEncoding: 'base64',
    ContentType: contentType,
    ACL: 'public-read'
  };

  return new Promise( resolve => {
    s3bucket.putObject(params, (error, data) => {
      if (error) {
        console.log(error);
      } else {
        console.log("successfully uploaded to s3");
      }

      resolve(BUCKET_PATH_URL + encodeURIComponent(key));
    });
  });
};

module.exports.uploadScene = function (imageData){
  let buf = new Buffer(imageData, 'base64');
  let imageName = 'scene-gen/' +  uuidv4() + '.png';
  var params = {
    Bucket: BUCKET_NAME,
    Key: imageName,
    Body: buf,
    ContentEncoding: 'base64',
    ContentType: 'image/png',
    ACL: 'public-read'
  };

  return new Promise( resolve => {
    s3bucket.putObject(params, (error, data) => {
      if (error) {
        console.log(error);
      } else {
        console.log("successfully uploaded to s3");
      }

      let url = BUCKET_PATH_URL + encodeURIComponent(imageName);
      console.log(url);
      resolve(url);
    });
  });
};

module.exports.getEmailPreview = key => {
  console.log("In getEmailPreview for " + key);
  return new Promise( resolve => {
    s3bucket.listObjects({
      Bucket: BUCKET_NAME,
      //Delimiter: '/',
      Prefix: key,
    }, (err, data) => {
      if(err){
        console.log("error in getEmailPreview - " + err);
      }else{
        resolve(BUCKET_PATH_URL + encodeURIComponent(data.Contents[0].Key));
      }
    })
  });
};




