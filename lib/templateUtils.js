let axios = require('axios');
const { createCanvas, loadImage, Image } = require('canvas')

async function getBinaryImage(src){
  let resp = await axios.request({
    responseType: 'arraybuffer',
    url: src,
    method: 'get',
  });
  return resp.data;
}

module.exports = class TemplateUtils{

  /**
   * Knock out white backgrounds
   *
   * TODO: make this smarter so that only consecutive white (or any color we choose)
   *       is removed
   */
  static knockOutBackground(image){
    const canvas = createCanvas(image.width, image.height);
    const ctx = canvas.getContext('2d')

    ctx.drawImage(image, 0, 0);

    var imgd = ctx.getImageData(0, 0, canvas.width, canvas.height),
      pix = imgd.data,
      newColor = {r:0,g:0,b:0, a:0};

    for (var i = 0, n = pix.length; i <n; i += 4) {
      var r = pix[i],
        g = pix[i+1],
        b = pix[i+2];

      // If its white then change it
      if(r >= 250 && g >= 250 && b >= 250){ 
        // Change the white to transparent.
        pix[i] = newColor.r;
        pix[i+1] = newColor.g;
        pix[i+2] = newColor.b;
        pix[i+3] = newColor.a;
      }
    }

    ctx.putImageData(imgd, 0, 0);
    return canvas.toDataURL("image/png");
  }

  /**
   * Generate a Scene
   */
  static async genScene(type, background, src){
    const canvas = createCanvas(1200, 628)
    const ctx = canvas.getContext('2d')


    if(/white|black|red|blue|green/.test(background)){
      ctx.fillStyle = background;
      ctx.fillRect(0,0,1200,628);
    }else if(background === 'wood'){
      let wood = await getBinaryImage('https://unsplash.com/photos/gILHZBtL_JA/download?force=true');
      let bkg = new Image();
      bkg.src = wood;
      ctx.drawImage(bkg, 0, 0, canvas.width, canvas.height);
    }

    let layer1 = new Image();
    layer1.src = await getBinaryImage(src);

    if(background !== 'white'){
      layer1.src = this.knockOutBackground(layer1);
      console.log("layer 1 source: " + layer1.src);
    }

    ctx.drawImage(
      layer1,
      canvas.width/2 - layer1.width/2,
      canvas.height/2 - layer1.height/2
    );

    return canvas.toBuffer().toString('base64');
  }


    static async phoneCrop(cropObject){

        // create image
        let imageWidth = cropObject.imageWidth;
        let imageHeight = cropObject.imageHeight;

        const imageCanvas = createCanvas(imageWidth, imageHeight);
        let ctx = imageCanvas.getContext('2d');
        let imageResult = new Image();
        imageResult.src = cropObject.imageData;
        ctx.drawImage(imageResult, 0, 0, imageWidth, imageHeight);
        let second = imageCanvas.toDataURL('image/png', 1.0);
        imageResult.src = second;
        console.log(second);

        let phoneWidth = cropObject.phoneWidth;
        let phoneHeight = cropObject.phoneHeight;

        const phoneCanvas = createCanvas(phoneWidth, phoneHeight);
        ctx = phoneCanvas.getContext('2d');

        console.log(imageResult.src);

        let leftClip = cropObject.left;
        let topClip = cropObject.top;
        let leftPixel = 0;
        let topPixel = 0;

        let clipWidth = phoneWidth;
        let clipHeight = phoneHeight;

        if (leftClip < 0){
            leftPixel = -1 * leftClip;
            leftClip = 0;

            if (leftPixel + imageWidth < phoneWidth){
                clipWidth = imageWidth;
                // phoneWidth = imageWidth;
            }
        }
        else{
            if ((-1 * leftClip) + imageWidth < phoneWidth){
                clipWidth = imageWidth - leftClip;
            }
        }

        if (topClip < 0){
            topPixel = -1 * topClip;
            topClip = 0;

            if (topPixel + imageHeight < phoneHeight){
                clipHeight = imageHeight;
                // phoneHeight = imageHeight;
            }
        }
        else{
            if ((-1 * topClip) + imageHeight < phoneHeight){
                clipHeight = imageHeight - topClip;
            }
        }



    ctx.drawImage(imageResult, leftClip, topClip, clipWidth, clipHeight, leftPixel, topPixel, clipWidth, clipHeight);
    return phoneCanvas.toDataURL('image/png', 1.0);
  }


  static async phoneMaskRecolor(url){
    let image = await getBinaryImage(url);
    console.log("url is: " + url);

    let img = new Image();
    return new Promise( resolve => {

      img.onload = (e) => {
        console.log("loaded!");
        console.log("image WxH = " + img.width + 'x' + img.height);

        const canvas = createCanvas(img.width, img.height);
        const ctx = canvas.getContext('2d')

        ctx.drawImage(img, 0, 0, canvas.width, canvas.height);

        let imgd = ctx.getImageData(0, 0, canvas.width, canvas.height);
        let pix = imgd.data;
        let transparent = {r:0,g:0,b:0, a:0};

        for (var i = 0, n = pix.length; i <n; i += 4) {
          var r = pix[i],
            g = pix[i+1],
            b = pix[i+2],
            a = pix[i+3];

          // If its on the red spectrum change it to transparent
          if(r >= 200 && g < 200 && b > 200){ 
            pix[i] = transparent.r;
            pix[i+1] = transparent.g;
            pix[i+2] = transparent.b;
            pix[i+3] = transparent.a;
          }

          // Gray should go to full gray
          if(r >= 220 && g > 220 && b > 200 && a > 1){ 
            pix[i] = 240;
            pix[i+1] = 240;
            pix[i+2] = 240;
            pix[i+3] = 150;
          }
        }

        ctx.putImageData(imgd, 0, 0);
        //resolve(canvas.toDataURL("image/png"));
        resolve(canvas.toBuffer());
      };

      img.src = image;
    });
    
  };

}


