var Spreadsheet = require('edit-google-spreadsheet');

module.exports = class ResearchSignup{

  static addSignup(data){

    return new Promise( (resolve) => {
      Spreadsheet.load({
        debug: true,
        spreadsheetId: '1Kt_-tkbQg6X6JWxlL0g4WwJgMs_8cUgvCqMVW-5d374',
        worksheetName: 'sign-ups',

        oauth : {
          email: 'hatchery-1370@appspot.gserviceaccount.com',
          key: '-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDtu8mgTzCznVpA\nc/afvJDcbEVsQKIi5A90pDd6qDrHQYL7GLKnXi8y/gHT+n8eBjLKx5NBWICkLQL5\ndFW4uizqTkmB2HQFqCvd3tcb3Sj4vhj0QrHzOV8Oyna2aVZ2Ep/z7E3ADTn9/JRd\nF9/mudsPZUWtnyRczNulr0usGrk+59i4SKM4jQ4MW5wMUR5g55L5CPPeZDotRD5h\nwRc18NE2ik7etx/XX9Y/O5o55KDezNU7918EMc9HG3B/DxLtjGroU3S8RotZ3UFP\noQzH89wzk06f4v0rZxPZOHXIRzz++IbLdb8YsyylOFoXQZXe9RTWEm1sg1Xp6jDA\nQG4M+ulHAgMBAAECggEBAOm8b0G5emWueNTT7xAggsDlhuAnMAJj/jotVwq2qovM\nAm7EWqqptHhLOxrit+aaK4gp7sMZs+k3BCylT76+9XgtHqKWYYRKU07iUNZpfGYL\n6VcG2BUOPEK7lzpDmSmfqDpYBB2mI3Vvx6M73PzpkNKzPG23ATZoiDB/iEQfRjDs\nN1B2Y8zWUYj+udwecJOdm9p2xEY387LZ+npk5u+CcFeR5zvfuH41ugpMTYRpqWZV\ntFFlBo6h8NRNOuvgmveiUDptixn4lIYuc4likEqCzVjXF4X/MhXSE2XaDiTMd0Ng\nrTUzJVm70shtTgj+BhQVS4GyVRt0I+kLB5hfMjOab6kCgYEA/tOreHvkm5lBep6A\nIhXPQFyKpfgsNmdwwX7SIW/681pOv/cNutinX9mALkAYnQeaMWyKzhputFhDomVw\nO3REj+w6gCyANmxaJ7ypTkcPwMiMDyQJlYWcSrZuIgMpwpFvsIdQjrXOPeVznjTI\nU28Aob7zS8qk0OAUwYoG3uZjUf0CgYEA7tP44/UFvU68d8A5GV4UyvYEtjfjBlUH\nsxvB9/HjaqjE+yBSHkWgMMI0yiMMP0bSnCdlkuOaHS1AJaD0W2l6nW2dZaKQdb3g\nQHt3kpbz5XdZzi5454nOQt1kaHEq08b2gltk0oJEsRJBZ5q0ilIAQEGAj9shiuz4\nW8Y6FttvuZMCgYAv7FTSCtK6ZH8I9Wf5gQqB+yVkAyEldhyhdx1DhpC2VWR7+5VM\nb1RC1V+i0xEN7xETfGs6SYRKTFMj5lnOBJakTnPOCRBvq77kvDlG7FxNECou6sMW\nqJL1Bgv+P4QKAFn14+zFefF4eH9e4GRQNI703DlbJpgKZCnjH/cc0k0ZbQKBgFTa\nwYKUyu5A7FuL03283JxkHrZzUtksGuk7QoaKzedWgW5MRID28fyA8+qh2zccPbY3\nlTJEZbxaGIGGF2vnJSaMDLFQugzrYqLme0x9D7kG18SW5GQ3pS5CpOsq0jGebnVQ\nbZkRqcBUISLjZ6QBkI9D9p6mF9vZaECLRJZcbuy7AoGBAKQduOce+aU/d552vX/i\nQn9xUvMr+y9yRYeKmU2FS8Skg9RBIiUFmWBORFhjnYgoN+rTYwbBP7ahqDQ+IC4O\nBuQss8u46UKRkT6OzABvbY95siUuMqZuLaR7WG+Gm6nrpo7CYItC+MzfZoJ/bumL\n46CCSI2t/DMLPEwiXzvOrT6p\n-----END PRIVATE KEY-----\n'
        }

      }, function sheetReady(err, spreadsheet) {

        if (err) {
          throw err;
        }

        spreadsheet.receive(function(err, rows, info) {
          if (err) {
            throw err;
          }

          var row = info.lastRow + 1;
          var obj = {};
          obj[row] = [[data['name'],data['business'],data['age'],data['gender'],data['interest'],data['email'],data['phone'],data['responsible'],data['sign-up-type'],data['date']]];

          spreadsheet.add(obj);

          spreadsheet.send(function(err) {
            if (err) {
              throw(err);
            }

            resolve();
          });

        });
      });
    });
  }
}
