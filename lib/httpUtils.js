var https = require('https');

module.exports.getSSLCertificate = function (host){
  var options = {
    host: host,
    port: '443',
    method: 'GET',
    rejectUnauthorized: false,
    agent: false,
  };

  return new Promise( (resolve) => {
    var req = https.request(options, function(res) {
      var certificateInfo = res.connection.getPeerCertificate();
      const days_left = Math.floor((new Date(certificateInfo.valid_to) - new Date()) / 1000 / 60 / 60 / 24);

      resolve({
        cn: certificateInfo.subject.CN,
        issuer: certificateInfo.issuer.O,
        valid_from: certificateInfo.valid_from,
        valid_to: certificateInfo.valid_to,
        days_left,
      });
    });

    req.end();
  });
};
