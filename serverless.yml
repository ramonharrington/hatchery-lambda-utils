# Welcome to Serverless!
#
# This file is the main config file for your service.
# It's very minimal at this point and uses default values.
# You can always add more config options for more control.
# We've included some commented out config examples here.
# Just uncomment any of them to get that config option.
#
# For full config options, check the docs:
#    docs.serverless.com
#
# Happy Coding!

service: hatchery-lambda-utils

# You can pin your service to only deploy with a specific Serverless version
# Check out our docs for more details
# frameworkVersion: "=X.X.X"

provider:
  name: aws
  runtime: nodejs8.10
  timeout: 300
  memorySize: 1024
  apiKeys:
    - ${opt:stage, self:provider.stage}-smsKey
  iamRoleStatements:
    - Effect: "Allow"
      Action:
        - dynamodb:Query
        - dynamodb:Scan
        - dynamodb:GetItem
        - dynamodb:PutItem
        - dynamodb:UpdateItem
        - dynamodb:DeleteItem
      Resource: "arn:aws:dynamodb:*:*:*"
    - Effect: "Allow"
      Action:
        - "sns:Publish"
      Resource: "*"

plugins:
  - serverless-offline
  - serverless-apigwy-binary
  - serverless-domain-manager
  - serverless-dynamodb-local
  - serverless-prune-plugin
custom:
  stage: ${opt:stage, self:provider.stage}
  customDomain:
    domainName: utils.hatchery.vistaprint.com
    createRoute53Record: true
    basePath: ${opt:stage, self:provider.stage}
  prune:
    automatic: true
    number: 3

resources:
  Resources:
    ApiGatewayRestApi:
      Properties:
        BinaryMediaTypes:
          - "image*"
          - "*~1*"

# Image generation and manipulation
functions:
  phoneCrop:
        handler: handlers/imagegen.phoneCrop
        events:
          - http:
              method: post
              path: /scene/phoneCrop
              cors: true


  getProductScene:
    handler: handlers/imagegen.getProductScene
    events:
      - http:
          method: get
          path: /scene/product
          cors: true
          contentHandling: CONVERT_TO_BINARY
          request: 
            parameters: 
              paths: 
                id: true

  getPhoneMask:
    handler: handlers/imagegen.getPhoneMask
    events:
      - http:
          method: get
          path: /masks/phone
          cors: true
          request:
            parameters:
              paths:
                id: true

# Misc Endpoints
  checkSSL:
    handler: handlers/http.checkSSL
    events:
      - http:
          method: get
          path: /http/ssl
          cors: true

  corsit:
    handler: handlers/http.corsit
    events:
      - http:
          method: any
          path: /http/corsit
          cors: true
  
  corsimage:
    handler: handlers/http.corsimage
    events:
      - http:
          method: get
          path: /http/corsimage
          cors: true
          contentHandling: CONVERT_TO_BINARY

## Facebook Endpoints
  fbSearch:
    handler: handlers/fb.search
    events:
      - http:
          method: get
          path: /fbbiz/search
          cors: true
  fbPlace:
    handler: handlers/fb.place
    events:
      - http:
          method: get
          path: /fbbiz/place
          cors: true

## Email Endpoints
  continueFromAnotherDevice:
    handler: handlers/email.continueFromAnotherDevice
    vpc:
      securityGroupIds:
        - sg-5744332d
      subnetIds:
        - subnet-61866c3a
        - subnet-9853f7d1
    events:
      - http:
          method: post
          path: /emails/continueFromAnotherDevice
          cors: true

  continueFromAnotherDeviceRedirect:
    handler: handlers/email.redirectEditLink
    events:
      - http:
          method: get
          path: /emails/redirectEditLink
          cors: true

  slackMailHook:
    handler: handlers/email.slackMailHook

  slackUTHook:
    handler: handlers/email.slackUsertestingHook

  sparkpost:
    handler: handlers/email.sparkpostIncoming
    vpc:
      securityGroupIds:
        - sg-5744332d
      subnetIds:
        - subnet-61866c3a
        - subnet-9853f7d1
    events:
      - http:
          method: post
          path: /emails/sparkpost

## Mobile Studio
  isOurStudio:
    handler: handlers/studio.isOurStudio
    events:
      - http:
          method: get
          path: /studio/isOurs
          cors: true
  recordStudio:
    handler: handlers/studio.setOurStudio
    events:
      - http:
          method: post
          path: /studio/record
          cors: true


## Montages / Transformations
  getCalendarMontage:
    handler: handlers/montage.getCalendar
    events:
      - http:
          method: get
          path: /montage/calendar
          cors: true
          request: 
            parameters: 
              paths: 
                altDocId: true

  createCalendarMontage:
    handler: handlers/montage.calendar
    events:
      - http:
          method: post
          path: /montage/calendar
          cors: true
          request: 
            parameters: 
              paths: 
                altDocId: true

  createXsellMontage:
    handler: handlers/montage.createXsellMontage
    events:
      - http:
          method: post
          path: /montage/xsell
          cors: true

  getXsellMontage:
    handler: handlers/montage.getXsellMontage
    events:
      - http:
          method: get
          path: /montage/xsell
          cors: true


# Research signup
  researchSignup:
    handler: handlers/researchSignup.researchSignup
    events:
      - http:
          method: post
          path: /research/signup
          cors: true



# Content Proxy
  cpPhonePP:
    handler: handlers/contentProxy.cpPhonePP
    events:
      - http:
          method: get
          path: /cproxy/phone


# SMS
  sendSMS:
    handler: handlers/sms.send
    events:
      - http:
          method: post
          path: /sms/send
          private: true
