# hatchery-lambda-utils

This package uses serverless.
https://serverless.com/framework/docs/providers/aws/guide/quick-start/

## Search pages/places

**URL** : https://t5gu5xdn0m.execute-api.us-east-1.amazonaws.com/dev/search

**Method** : `GET`

**Params**
* string **query**
    * required
    * the keyword for the search
* string **type**
    * required
    * either a place (has an address and uses the latitude and longitude params to filter) or page (no address, so you can get any result)
* string **lat**
    * defaults to empty if not passed
    * the latitude to filter location based results.
    * if passed, lon needs to be passed as well
* string **lon** 
    * defaults to empty if not passed
    * the latitude to filter location based results
    * if passed, lat needs to be passed as well
* boolean **geolocate**
    * set to 1 to pass latitude & longitude of the IP performing the request
    * overwrites manually setting **lat** and **lon**
* string **after**
    * defaults to empty if not passed
    * pointer id to the next page
* string **before**
    * defaults to empty if not passed
    * pointer id to the previous page
* string **limit** 
    * defaults to 5
    * max number of results 

## Get page details

**URL** : https://t5gu5xdn0m.execute-api.us-east-1.amazonaws.com/dev/place

**Method** : `GET`

**Params**
* string **pageId**
    * required
    * id of the page from the search results

#### notes
* Facebook returns a ton of information for a business/place. We have limited that for now to only the fields that were relevant to us.
